package games.war;

import playingCardParts.StandardCard;
import deckParts.Deck;


public class War {
	public static Deck<StandardCard> playerHand; 
	public static Deck<StandardCard> computerHand; 
	public static StandardCard playerCard; 
	public static StandardCard computerCard; 

	public War(){}

	public void playWar(){
		WarSetUp.setUp(); 

		while (playerHand.size() > 0 && computerHand.size()>0)
			playBattle();

		declareWinner(); 
	}

	public void playBattle(){	
		Deck<StandardCard> winnersCards = new Deck<StandardCard>();

		setUpSingleBattle(winnersCards); 
		battleAndAddWinnersCards(winnersCards); 
	}

	public void setUpSingleBattle(Deck<StandardCard> winnersCards){
		playerCard = playerHand.dealTopCard(); 
		computerCard = computerHand.dealTopCard();

		winnersCards.combine(playerCard); 
		winnersCards.combine(computerCard); 
	}

	public void battleAndAddWinnersCards(Deck<StandardCard> winnersCards){
		int rankDiff = getCardRankDifference();
		battle(rankDiff, winnersCards);
	}

	public void battle(int rankDiff, Deck<StandardCard> winnersCards){
		if (rankDiff > 0)
			playerWon(winnersCards);
		else if (rankDiff < 0)
			computerWon(winnersCards);
		else
			declareWar(winnersCards);
	}

	public int getCardRankDifference(){
		return  playerCard.compareRank(computerCard); 
	}

	public void play3CardWarBattle(Deck<StandardCard> winnersCards){
		if(!checkAndSetUpWar(winnersCards)){
			emptyLoserHand(); 
			return;
		}
		battleAndAddWinnersCards(winnersCards); 
	}

	public boolean checkAndSetUpWar(Deck<StandardCard> winnerHand){
		boolean isWarPossible = checkIfWarIsPossible(); 
		if(checkIfWarIsPossible())
			setUp3CardWarBattle(winnerHand);
		return isWarPossible; 
	}

	public boolean checkIfWarIsPossible(){
		boolean isPossibleForPlayer = (playerHand.size() >= 4) ? true : false;
		boolean isPossibleForComputer = (computerHand.size() >= 4) ? true : false; 
		return isPossibleForPlayer && isPossibleForComputer; 
	}

	public void setUp3CardWarBattle(Deck<StandardCard> winnersCards){
		setUpCardsDownInWar(winnersCards);
		setUpSingleBattle(winnersCards); 
	}

	public void setUpCardsDownInWar(Deck<StandardCard> winnersCards){
		Deck<StandardCard> playerCardsDown = playerHand.deal(3); 
		Deck<StandardCard> computerCardsDown = computerHand.deal(3);

		winnersCards.combine(playerCardsDown); 
		winnersCards.combine(computerCardsDown);
	}

	private void emptyLoserHand(){
		if (playerHand.size() >= computerHand.size())
			playerHand.combine(computerHand); 
		else
			computerHand.combine(playerHand); 
	}

	private void declareWinner() {
		String winner = (playerHand.size() >= computerHand.size()) ? "You are" : "Computer is"; 
		System.out.println(playerHand.size()+"/" +computerHand.size() + winner + " the winner!"); 
	}

	public void playerWon(Deck<StandardCard> winnersCards){
		System.out.println(playerHand.size()+"/" +computerHand.size() + 
				" You play " + playerCard.toString() + " and computer plays " + computerCard.toString() + " You won!");
		playerHand.combine(winnersCards); 
	}

	public void computerWon(Deck<StandardCard> winnersCards){
		System.out.println(playerHand.size()+"/" +computerHand.size() + 
				" You play " + playerCard.toString() + " and computer plays " + computerCard.toString() + " Computer wins. :(");
		computerHand.combine(winnersCards); 
	}

	public void declareWar(Deck<StandardCard> winnersCards){
		System.out.println(playerHand.size()+"/" +computerHand.size() + " WAR!!!" +
				" You play " + playerCard.toString() + " and computer plays " + computerCard.toString() + "."); 
		play3CardWarBattle(winnersCards); 
	}
}

package games.war;

import playingCardParts.StandardCard;
import deckParts.Deck;

public class WarSetUp {
	
	public static void setUp(){
		Deck<StandardCard> aDeck = setUpWarDeck(); 
		setUpWarHands(aDeck); 
	}
	
	private static Deck<StandardCard> setUpWarDeck(){		
		Deck<StandardCard> aDeck = new Deck<StandardCard>();
		aDeck.initFullDeck();
		aDeck.shuffle(); 		
		return aDeck; 
	}
	
	private static void setUpWarHands(Deck<StandardCard> aDeck){
		War.playerHand = aDeck.deal(26); 
		War.computerHand = aDeck; 
	}
}

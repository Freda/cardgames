import java.util.Scanner;

import playingCardParts.StandardCard;
import playingCardParts.Attributes.TraditionalSuit;
import games.war.War;
import deckParts.*;


public class Runner {

	public static void runBasicDeckFunctions(){
		Deck<StandardCard> basicDeck = createAndShuffleDeck(); 
		modifyDeck(basicDeck); 
		createAHandAndCompare2Cards(basicDeck);
	}

	public static Deck<StandardCard> createAndShuffleDeck(){
		Deck<StandardCard> basicDeck = createBasicDeck();
		shuffleDeck(basicDeck);
		return basicDeck;
	}

	public static Deck<StandardCard> createBasicDeck(){
		Deck<StandardCard> standardDeck = new Deck<StandardCard>(); 
		standardDeck.initFullDeck();
		System.out.println("We are just created a new deck. They include the cards: \n" + standardDeck.toString()); 
		return standardDeck;
	}

	public static void shuffleDeck(Deck<StandardCard> deck){
		deck.shuffle();
		System.out.println("We are just shuffled the deck. The new order is: \n" + deck.toString()); 
	}

	public static void modifyDeck(Deck<StandardCard> deck){
		keepOnlyHearts(deck); 
		System.out.println("We are just modified the deck by removing all spades, diamonds and clubs. The new deck is: \n" + deck.toString()); 
	}

	public static void keepOnlyHearts(Deck<StandardCard> deck){
		deck.removeAll(TraditionalSuit.SPADES);
		deck.removeAll(TraditionalSuit.DIAMONDS);
		deck.removeAll(TraditionalSuit.CLUBS);
	}

	public static void createAHandAndCompare2Cards(Deck<StandardCard>basicDeck){
		Deck<StandardCard> hand = dealHand(basicDeck);
		take2CardsAndCompare(hand); 
	}

	public static Deck<StandardCard> dealHand(Deck<StandardCard> deck){
		Deck<StandardCard> hand = deck.deal(5);
		System.out.println("We are now going to create a hand of 5 from the modified deck. The new hand is " +hand.toString());
		return hand; 
	}
	
	public static void take2CardsAndCompare(Deck<StandardCard> hand){
		StandardCard firstCard = takeACard(hand); 
		StandardCard secondCard = takeACard(hand);
		System.out.println("From the hand, we are picking " + firstCard + " and " + secondCard); 
		compare2Cards(firstCard, secondCard); 
	}
	
	public static StandardCard takeACard(Deck<StandardCard> deck){
		return deck.dealTopCard();
	}
	
	public static void compare2Cards(StandardCard firstCard, StandardCard secondCard){
		StandardCard biggerCard = (firstCard.compareRank(secondCard) >0) ? firstCard : secondCard; 
		System.out.println("We will now compare the ranks of the cards. The " + biggerCard + " is bigger."); 
	}

	public static void askForGameAndPlay(){
		Scanner reader = new Scanner(System.in);
		System.out.println("\nDo you want to play a game of War? Enter Y/N:"); 
		String input = reader.next();
		
		if (input.trim().equalsIgnoreCase("Y")){
			playWar(); 
		}
	}
	
	public static void playWar(){
		War aWar = new War();
		aWar.playWar(); 
	}

	public static void main(String[] args) {
		System.out.println("This is a little class to create a deck and run through some of the basic deck functions.");
		runBasicDeckFunctions(); 
		askForGameAndPlay();
	}

}

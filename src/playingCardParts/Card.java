package playingCardParts;
import playingCardParts.Attributes.Color;
import playingCardParts.Attributes.Rank;
import playingCardParts.Attributes.Suit;

public abstract class Card {

	public Rank rank;
	public Suit suit;
	public Color color; 
	
	public Card(Rank aRank, Suit aSuit){
		this.rank = aRank;
		this.suit = aSuit;
	}
		
	public int compareRank(Card aCard){
		return rank.compare(aCard.rank);
	}
	
	public int compareSuit(Card aCard){
		return suit.compare(aCard.suit);
	}
	
	public int compareColor(Card aCard){
		return color.compare(aCard.color);
	}
	
	public int getRank(){
		return rank.getRank();
	}
	
	public int getSuit(){
		return suit.getSuit();
	}
	
	public int getColor(){
		return color.getColor();
	}
	
	public String toString(){
		return rank + " of " + suit; 
	}
}

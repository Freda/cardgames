package playingCardParts;

import playingCardParts.Attributes.Color;
import playingCardParts.Attributes.SpecialRank;
import playingCardParts.Attributes.SpecialSuit;

public class Joker extends Card{

	public Joker(Color color){
		super(SpecialRank.JOKER, SpecialSuit.JOKER); 
		this.color = color; 
	}

	@Override
	public String toString(){
		return color + " " + suit; 
	}
}

package playingCardParts;

import playingCardParts.Attributes.TraditionalColor;
import playingCardParts.Attributes.TraditionalRank;
import playingCardParts.Attributes.TraditionalSuit;

public class StandardCard extends Card{

	
	public StandardCard(TraditionalRank aRank, TraditionalSuit aSuit){
		super(aRank, aSuit); 
		assignColor(); 
	}
	
	private void assignColor(){
		switch ((TraditionalSuit) suit){
		case HEARTS:
		case DIAMONDS:
			this.color = TraditionalColor.RED; 
			break;
		default:
			this.color = TraditionalColor.BLACK; 
		}
	}
	
}

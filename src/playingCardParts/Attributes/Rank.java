package playingCardParts.Attributes;

public interface Rank {
	
	public int getRank();
	public void setRank(int value); 
	public int compare(Rank rank);
}

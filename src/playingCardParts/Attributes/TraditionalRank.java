package playingCardParts.Attributes;


public enum TraditionalRank implements Rank {
	ACE (14),
	KING (13),
	QUEEN (12),
	JACK (11),
	TEN (10),
	NINE (9),
	EIGHT (8),
	SEVEN (7),
	SIX (6),
	FIVE (5),
	FOUR (4),
	THREE (3),
	TWO (2);
	
	public int value; 
	
	TraditionalRank(int value){
		this.value = value; 
	}
	
	public void setRank(int rank){
		this.value = rank; 
	}
	
	public int getRank(){
		return value; 
	}
	
	public int compare(Rank aRank){
		return value - aRank.getRank();
	}
	
}

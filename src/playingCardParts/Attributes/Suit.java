package playingCardParts.Attributes;

public interface Suit {
	
	public int getSuit();
	public void setSuit(int value); 
	public int compare(Suit aSuit);
}

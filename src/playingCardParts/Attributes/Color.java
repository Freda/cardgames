package playingCardParts.Attributes;

public interface Color { 
	
	public void setColor(int value);
	public int getColor();
	public int compare(Color aColor);
}

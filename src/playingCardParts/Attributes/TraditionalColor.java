package playingCardParts.Attributes;


public enum TraditionalColor implements Color{
	BLACK (2),
	RED (1);
	
	public int value; 
	
	TraditionalColor(int value){
		this.value = value; 
	}
	
	public void setColor(int value){
		this.value = value; 
	}
	
	public int getColor(){
		return value; 
	}
	
	public int compare(Color aColor){
		int difference = value - aColor.getColor(); 
		return difference; 
	}
}

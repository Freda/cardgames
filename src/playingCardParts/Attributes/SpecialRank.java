package playingCardParts.Attributes;


public enum SpecialRank implements Rank {
	JOKER (0),
	COVER (-1),
	INSTRUCTION (-1),
	BLANK (-1); 
	
	public int value; 
	
	SpecialRank(int value){
		this.value = value; 
	}
	
	public void setRank(int rank){
		this.value = rank; 
	}
	
	public int getRank(){
		return value; 
	}
	
	public int compare(Rank aRank){
		int difference = value - aRank.getRank(); 
		return difference; 
	}
}

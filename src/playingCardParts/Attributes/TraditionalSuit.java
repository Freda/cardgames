package playingCardParts.Attributes;


public enum TraditionalSuit implements Suit{
	SPADES (4),
	HEARTS (3),
	DIAMONDS (2),
	CLUBS (1); 
	
	public int value; 
	
	TraditionalSuit(int value){
		this.value = value; 
	}
	
	public void setSuit(int rank){
		this.value = rank; 
	}
	
	public int getSuit(){
		return value; 
	}
	
	public int compare(Suit aSuit){
		int difference = value - aSuit.getSuit(); 
		return difference; 
	}
}

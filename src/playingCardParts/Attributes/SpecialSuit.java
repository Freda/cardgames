package playingCardParts.Attributes;


public enum SpecialSuit implements Suit{
	JOKER (0),
	NONE (-1); 
	
	public int value; 
	
	SpecialSuit(int value){
		this.value = value; 
	}
	
	public void setSuit(int rank){
		this.value = rank; 
	}
	
	public int getSuit(){
		return value; 
	}
	
	public int compare(Suit aSuit){
		int difference = value - aSuit.getSuit(); 
		return difference; 
	}
}

package deckParts;

import playingCardParts.Card;
import playingCardParts.Joker;
import playingCardParts.Attributes.Color;
import playingCardParts.Attributes.TraditionalColor;

public class DeckWJokers extends Deck<Card>{

	public DeckWJokers() {
		super();
		addJokers(); 
	}
	
	private void addJokers(){
		for(Color aColor : TraditionalColor.values()){
			Joker aJoker = new Joker(aColor); 
			combine(aJoker); 
		}
	}
}

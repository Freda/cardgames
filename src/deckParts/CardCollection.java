package deckParts;

import playingCardParts.Attributes.Color;
import playingCardParts.Attributes.Rank;
import playingCardParts.Attributes.Suit;


public interface CardCollection<E>{ 
	
	public void shuffle(); 
	public CardCollection<E> deal(int removeNum);
	public void removeAll(Suit suit);
	public void removeAll(Rank rank);
	public void removeAll(Color color);
}

package deckParts;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import playingCardParts.*;
import playingCardParts.Attributes.*;


public class Deck<E extends Card> implements CardCollection<E>{
	
	public LinkedList<E> cards= new LinkedList<E>(); ; 
	
	public Deck(){}
	
	//O(n^2)
	public void initFullDeck() {
		for (TraditionalSuit suit : TraditionalSuit.values()){
			for (TraditionalRank rank : TraditionalRank.values()){
				Card aCard = new StandardCard(rank, suit); 
				cards.add((E) aCard); 
			}
		}
	}
	
	public void shuffle() {
		Collections.shuffle(cards); 
	}

	//O(n)
	public Deck<E> deal(int removeNum) {
		Deck<E> deck = new Deck<E>(); 
		Iterator<E> it = cards.iterator();
		
		while(it.hasNext() && removeNum > 0)
		{
		    E aCard = it.next();
		    deck.combine(aCard);
		    it.remove();
		    removeNum--; 
		}
		
		return deck;
	}
	
	//O(1)
	public <E> E dealTopCard() {
		assert cards.size() >= 1; 
		return (E) cards.remove(0); 
	}
	
	//O(n)
	public <E> E dealFromBottom() {
		assert cards.size() >= 1;
		return (E) cards.removeLast(); 
	}
	
	//O(n)
	public boolean hasCard(Card aCard){
		return cards.contains(aCard); 
	}
	
	//O(1)
	public void combine(E aCard){
		cards.add(aCard); 
	}
	
	//O(n)
	public void combine(Deck<E> aDeck) {
		Iterator<E> it = aDeck.cards.iterator();
		
		while(it.hasNext())
		{
		    E aCard = it.next();
		    combine(aCard);
		    it.remove();
		}
	}
	
	//O(n)
	public void removeAll(Suit suit) {
		Iterator<E> it = cards.iterator();
		
		while(it.hasNext())
		{
		    E aCard = it.next();
		    int cardsSuit = ((Card) aCard).getSuit();
		    if (cardsSuit == suit.getSuit())
		    	it.remove();
		}
	}

	public void removeAll(Rank rank) {
		Iterator<E> it = cards.iterator();
		
		while(it.hasNext())
		{
		    E aCard = it.next();
		    int cardsRank = ((Card) aCard).getRank();
		    if (cardsRank == rank.getRank())
		    	it.remove();
		}
	}

	public void removeAll(Color color) {
		Iterator<E> it = cards.iterator();
		
		while(it.hasNext())
		{
		    E aCard = it.next();
		    int cardsColor = ((Card) aCard).getColor();
		    if (cardsColor == color.getColor())
		    	it.remove();
		}
	}
	
	//O(n)
	public void removeSpecificCard(Card aCard){
		cards.remove(aCard); 
	}
	
	public String toString(){
		StringBuilder aString = new StringBuilder();  
		
		for (E aCard : cards)
			aString.append(aCard.toString() + ", "); 
		
		aString.append("\n"); 
		return aString.toString(); 
	}
	
	public int size(){
		return cards.size(); 
	}
}
